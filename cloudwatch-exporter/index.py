import json
import urllib3
import logging
import gzip
import re
from os import getenv
from base64 import b64decode
from random import randint
from socket import gethostname
from uuid import uuid1
from datetime import datetime, timezone
import os

runtime_region = os.environ['AWS_REGION']

pattern = r"[^a-zA-Z0-9]"

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger("logiq:lambda")
logger.setLevel(logging.DEBUG)


def lambda_handler(event, context):
    aws_service = "logiq_aws_exporter"
    try:
        # logger.info(event)
        aws_data = event["awslogs"]["data"]
        namespace = (
            getenv("NAMESPACE")
            if getenv("NAMESPACE") is not None
            else "logiq-lambda-exporter"
        )
        app_name = (
            getenv("APP_NAME")
            if getenv("APP_NAME") is not None
            else "logiq-app"
        )
        cluster_id = (
            getenv("CLUSTER_ID")
            if getenv("CLUSTER_ID") is not None
            else "logiq-cluster"
        )
        logiq_host = getenv("LOGIQ_HOST")
        if logiq_host is None:
            raise ValueError("No value for lLOGIQ_HOST provided")
        # logger.debug("namespace: {}, app_name: {}, cluster_id: {}, logiq_host: {}".format(namespace, app_name, cluster_id, logiq_host))
        ingest_token = getenv("INGEST_TOKEN")
        if ingest_token is None:
            raise ValueError("Auth token is missing!")
        payload_data = []
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(ingest_token),
        }
        if aws_data != None and aws_data != "":
            decoded_data = b64decode(aws_data)
            decompressed_data = gzip.decompress(decoded_data)
            if decompressed_data != None and decompressed_data != "":
                log_data = json.loads(decompressed_data)
                proc_id = "-"
                path = ""
                if "logStream" in log_data:
                    proc_id = log_data["logStream"].split("]")[-1]
                if "logGroup" in log_data:
                    path = log_data["logGroup"]
                    if "API-Gateway-Execution-Logs" in path:
                        aws_service = "api-gateway"
                    
                    if "/aws/lambda/" in path:
                        aws_service = "lambda"
                        
                        
                if "logEvents" in log_data:
                    log_events = log_data["logEvents"]
                    for log in log_events:
                        data = {
                            "@timestamp": datetime.fromtimestamp(
                                log["timestamp"] / 1000
                            )
                            .astimezone()
                            .isoformat(),
                            "@version": "0",
                            "cluster_id": re.sub(pattern, "-", cluster_id),
                            "application": re.sub(pattern, "-", app_name),
                            "proc_id": re.sub(pattern, "-", proc_id),
                            "host": gethostname(),
                            "message": log["message"],
                            "namespace": re.sub(pattern, "-", namespace),
                            "loggroup": path,
                            "logstream": log_data["logStream"],
                            "aws_service": aws_service,
                            "aws_region": runtime_region,
                        }
                        payload_data.append(data)

        encoded_data = json.dumps(payload_data).encode("utf-8")
        # logger.info(encoded_data)
        http = urllib3.PoolManager()
        r = http.request(
            "POST",
            "{}/v1/json_batch".format(logiq_host),
            body=encoded_data,
            headers=headers,
        )
        logger.info(r.status)
        return r.status
    except Exception as e:
        logger.error(e)
