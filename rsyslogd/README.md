# rsyslog

Rsyslogd is a system utility providing support for message logging. Support of both internet and unix domain sockets enables this utility to support both local and remote logging.

## rsyslog configuration
Use the below configuration for forwarding syslog to LOGIQ servers

### Installation
rsyslog is installed by default in most modern OS's, rsyslog needs the omrelp module to send data to a RELP aware endpoint such as LOGIQ. To enable RELP install packages listed below

 - rsyslog-relp, enables RELP protocol for rsyslog
```sh
sudo apt install rsyslog-gnutls rsyslog-relp
```

 - rsyslog-gnutls, enables rsyslog to communicate over secure socket 
```sh
sudo apt install rsyslog-gnutls rsyslog-relp
```

For Redhat/CentOS/Fedora, use yum to install the necessary packages

```sh
yum install rsyslog-gnutls rsyslog-relp```
```

#### Configuration Updates

Update the syslog config in ```/etc/rsyslog.conf``` or ```/etc/rsyslog.d/50-default.conf```

```sh

module(load="omrelp")
action(type="omrelp" 
        target="localhost" 
        port="2514" 
        tls="on" 
        tls.caCert="/LOGIQ/certs/LOGIQ.crt" 
        tls.myCert="/LOGIQ/certs/client.crt" 
        tls.myPrivKey="/LOGIQ/certs/client.key" 
        tls.authMode="fingerprint"
        tls.PermittedPeer=["SHA1:BF:46:AB:9F:A3:77:46:AF:6B:D2:EC:A4:30:72:F1:CC:0E:17:C9:42"]
        action.reportSuspensionContinuation="on"
        action.resumeRetryCount="-1"
        action.resumeInterval="10"
        queue.type="fixedArray"
        queue.size="250000"
        queue.dequeueBatchSize="1024"
        queue.workerThreads="4"
        queue.workerThreadMinimumMessages="50000"
        queue.spoolDirectory="/var/log/rsyslog"
        queue.fileName="XXX_sock"
        queue.maxFileSize="16m"
        queue.maxDiskSpace="2G"
        queue.highWaterMark="200000"
        queue.lowWaterMark="100000"
        queue.checkpointInterval="30000"
        queue.saveOnShutdown="on"
        queue.timeoutEnqueue="1"
)

```
### Minimal RELP without TLS



```
# Minimal config

$ModLoad imuxsock # provides support for local system logging
$ModLoad imklog   # provides kernel logging support
$ModLoad omrelp

$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat
$FileOwner root
$FileGroup root
$FileCreateMode 0640
$DirCreateMode 0755
$Umask 0022
$WorkDirectory /var/spool/rsyslog
$IncludeConfig /etc/rsyslog.d/*.conf

auth,authpriv.*			/var/log/auth.log
*.*;auth,authpriv.none		-/var/log/syslog
#cron.*				/var/log/cron.log
daemon.*			-/var/log/daemon.log
kern.*				-/var/log/kern.log
lpr.*				-/var/log/lpr.log
mail.*				-/var/log/mail.log
user.*				-/var/log/user.log

mail.info			-/var/log/mail.info
mail.warn			-/var/log/mail.warn
mail.err			/var/log/mail.err

news.crit			/var/log/news/news.crit
news.err			/var/log/news/news.err
news.notice			-/var/log/news/news.notice

*.=debug;\
	auth,authpriv.none;\
	news.none;mail.none	-/var/log/debug
*.=info;*.=notice;*.=warn;\
	auth,authpriv.none;\
	cron,daemon.none;\
	mail,news.none		-/var/log/messages

*.emerg				:omusrmsg:*

daemon.*;mail.*;\
	news.err;\
	*.=debug;*.=info;\
	*.=notice;*.=warn	|/dev/console

*.*  :omrelp:127.0.0.1:20514


```