# Kubernetes Logging with Fluent Bit
[Fluent Bit](http://fluentbit.io) is a lightweight and extensible __Log Processor__ that comes with full support for Kubernetes:


## Supported features ##

* One daemon of fluent-bit pod per node
* Read Kubernetes/Docker log files from the file system or through systemd Journal
* Enrich logs with Kubernetes metadata
* Separate namespace : apica-logging
* RBAC
* Define unique ClusterID inside fluent-bit-daemonset-logiq-forward.yml 

This repository contains a set of Yaml files to deploy Fluent Bit to your k8s cluster that considers namespace, RBAC, Service Account, etc.

## Getting started

[Fluent Bit](http://fluentbit.io) must be deployed as a DaemonSet so that it will be available on every node of your Kubernetes cluster. To get started run the following commands to create the namespace, service account and role setup:

## How to run ##

```sh
$ kubectl create namespace apica-logging
$ kubectl create -f fluent-bit-service-account.yaml 
$ kubectl create -f fluent-bit-role-binding.yaml 
$ kubectl create -f fluent-bit-role.yaml
```

#### Fluent Bit to LOGIQ

The next step is to create a ConfigMap that will be used by the Fluent Bit DaemonSet:

```
$ kubectl create -f fluent-bit-config-logiq-forward.yml
```

Fluent Bit DaemonSet is ready to be used with LOGIQ on a regular Kubernetes Cluster, configure the following in deamonset fluent-bit-daemonset-logiq-forward.yml 
          - name:  LOGIQ_HOST
            value: 34.94.61.50
          - name:  LOGIQ_PORT
            value: "24224"

For Kubernetes version < 1.17, please change the apiVersion: extensions/v1beta1 from apps/v1 and remove selector attached to DaemonSet app
  selector:
    matchLabels:
      k8s-app: fluent-bit-logging
```
$ kubectl create -f fluent-bit-daemonset-logiq-output.yml
```

When using Secure Forward mode, the TLS mode requires to be enabled. The following additional configuration parameters are available:

```
Key		Description								Default
Shared_Key	A key string known by the remote Fluentd used for authorization.	
Self_Hostname	Default value of the auto-generated certificate common name (CN).	
tls		Enable or disable TLS support						Off
tls.verify	Force certificate validation						On
tls.debug	Set TLS debug verbosity level. It accept the following values: 0 (No debug), 1 (Error), 2 (State change), 3 (Informational) and 4 Verbose	1
tls.ca_file	Absolute path to CA certificate file	
tls.crt_file	Absolute path to Certificate file.	
tls.key_file	Absolute path to private Key file.	
tls.key_passwd	Optional password for tls.key_file file.
```

These paremeter need to be overridden in configmap and deamonset
