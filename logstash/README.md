# README #

Configuration and scripts for integrating third party apps to use LOGIQ

### What is this repository for? ###

* How to configure logstash to talk to LOGIQ

### How do I get set up? ###

* Copy the specific configuration and edit for your specific setup
* LOGIQ is a secure server and allows communication via SSL/TLS only. Pl. have the correct certs in the configuration for logstash to be able to talk with LOGIQ.
* LOGIQ can process both RFC3164 and RFC5424 style messages, edit the syslog config to specify rfc as 'rfc3164' or 'rfc5424'

### Who do I talk to? ###

* Connect with us at https://logiq.ai
