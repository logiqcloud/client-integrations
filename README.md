# README #

Configuration and scripts for integrating third party apps to use LOGIQ

### What is this repository for? ###

* Code examples for clients
* Example configurations

### How do I get set up? ###

* Copy the examples for your client and modify.

### Who do I talk to? ###

* Connect with us at https://logiq.ai 
