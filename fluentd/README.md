# Running fluentd to sent to LOGIQ #

## Supported features ##

* One daemon pod of fluentd per node
* Pass client key pair, ca
* Separate namespace : apica-logging
* RBAC
* Define unique ClusterID inside fluentd-logiq.yaml

## How to run ##

```sh
> kubectl create namespace apica-logging
> kubectl apply -f fluentd_rbac.yaml
> kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default
> kubectl apply -f secrets.yaml
> kubectl apply -f fluentd-logiq.yaml

```

## FAQ ##

  * Can i run in a different namespace
    * Yes it is possible to run in your own custom namespace, edit the yaml files to use the namespace you would like to use
