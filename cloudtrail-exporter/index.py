import json
import urllib3
import logging
import gzip
import re
from os import getenv
from base64 import b64decode
from random import randint
from socket import gethostname
from uuid import uuid1
from datetime import datetime, timezone
import os

runtime_region = os.environ['AWS_REGION']

pattern = r"[^a-zA-Z0-9]"

logging.basicConfig(
    format="%(asctime)s - %(lineno)d  %(name)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger("logiq:lambda")
logger.setLevel(logging.DEBUG)



def lambda_handler(event, context):
    # logger.info(event)
    aws_service = "logiq_aws_exporter"
    try:
        logiq_host = getenv("LOGIQ_HOST")
        SSL = getenv("SSL")
        aws_data = event["awslogs"]["data"]
        namespace = (
            getenv("NAMESPACE")
            if getenv("NAMESPACE") is not None
            else "logiq"
        )
        app_name = (
            getenv("APP_NAME")
            if getenv("APP_NAME") is not None
            else "cloudtrail"
        )
        cluster_id = (
            getenv("CLUSTER_ID")
            if getenv("CLUSTER_ID") is not None
            else "aws"
        )
        
       
        if logiq_host is None:
            raise ValueError("No value for lLOGIQ_HOST provided")
        # logger.debug("namespace: {}, app_name: {}, cluster_id: {}, logiq_host: {}".format(namespace, app_name, cluster_id, logiq_host))
        ingest_token = getenv("INGEST_TOKEN")
        if ingest_token is None:
            raise ValueError("Auth token is missing!")
        payload_data = []
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(ingest_token),
        }
        if aws_data != None and aws_data != "":
            decoded_data = b64decode(aws_data)
            
            decompressed_data = gzip.decompress(decoded_data)
            if decompressed_data != None and decompressed_data != "":
                log_data = json.loads(decompressed_data)
                proc_id = "-"
                path = ""
                # logger.info(log_data)
                if "logStream" in log_data:
                    proc_id = log_data["logStream"].split("]")[-1]
                if "logGroup" in log_data:
                    path = log_data["logGroup"]
                    if "API-Gateway-Execution-Logs" in path:
                        aws_service = "api-gateway"
                    
                    if "/aws/lambda/" in path:
                        aws_service = "lambda"
                        
                        
                if "logEvents" in log_data:
                    log_events = log_data["logEvents"]
                    for log in log_events:
                        # logger.info("Print Events")
                        # logger.info(log)
                        message = json.loads(log['message'])
                        # logger.info(message)
                        # logger.info("Print Events")
                        # logger.info(flatten_json(message)) //TODO use this
                        data = flatten_json(message)
                        # logger.info(data)
                        data["@timestamp"] = datetime.fromtimestamp(
                                log["timestamp"] / 1000
                            )
                        data["cluster_id"] = re.sub(pattern, "-", cluster_id)
                        data["application"] = re.sub(pattern, "-", app_name)
                        data["cloudtrail"] = re.sub(pattern, "-", app_name)
                        data["proc_id"] = message["eventName"]
                        data["message"] = message["userAgent"]

                        payload_data.append(data)

        
        encoded_data = json.dumps(payload_data, default=str).encode("utf-8")
        # logger.info(encoded_data)
       
        http = urllib3.HTTPSConnectionPool(logiq_host,  cert_reqs='CERT_NONE', assert_hostname=False)
    
        r = http.request(
            "POST",
            "/v1/json_batch".format(logiq_host),
            body=encoded_data,
            headers=headers,
        )
        logger.info(r.status)
        return r.status

    except Exception as e:
        logger.error(e)


def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '.')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '.')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out