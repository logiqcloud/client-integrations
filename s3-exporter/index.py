import json
import urllib3
import logging
import gzip
import re
from os import getenv
import datetime
import boto3
from pathlib import Path

logging.basicConfig(
    format="%(asctime)s - %(lineno)d  %(name)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger("logiq:lambda")
logger.setLevel(logging.DEBUG)

event_sample = '{"Records":[{"eventVersion":"2.1","eventSource":"aws:s3","awsRegion":"us-east-1","eventTime":"2021-10-12T04:54:14.740Z","eventName":"ObjectCreated:Put","userIdentity":{"principalId":"AWS:AIDATVUMC2V2DSLHL3SCF"},"requestParameters":{"sourceIPAddress":"49.207.221.111"},"responseElements":{"x-amz-request-id":"J1MF8WVQM3VV3MNK","x-amz-id-2":"ilLmUB5U20VKvKdPrTD49m3BWZY0AjLvU6RDISV6PaMhWr2gegPNDNSg1DLoQjp9iwx7UAqHalcBX1IPSV8jMRdkehhWrN3nM6JEh5UAaMk="},"s3":{"s3SchemaVersion":"1.0","configurationId":"0e1852b7-364a-42e7-9beb-452e0b654434","bucket":{"name":"logiqmicrok8s","ownerIdentity":{"principalId":"A1YVOLJTA8X6GM"},"arn":"arn:aws:s3:::logiqmicrok8s"},"object":{"key":"some_logs/2021-10-08-17-13-21-2965DE2ADDAB1885.gz","size":84525,"eTag":"3245754fbeeb192a0aba6d6fcb908cec","sequencer":"00616514FE0D47B6A4"}}}]}'

VPC_FLOW_LOGS = 'vpc_flow_logs'

pattern = r"[^a-zA-Z0-9]"


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def detect_line(line):
    if ('interface-id' in line) and ('protocol' in line) and ('log-status' in line) and ('srcaddr' in line):
        return VPC_FLOW_LOGS


def process_vpc_flow_logs(line, headers):
    data = line.split()
    r_val = dict()
    for index, item in enumerate(headers):
        r_val[item] = data[index]

    r_val["message"] = r_val["log-status"]
    r_val["application"] = "vpc-flow-logs"
    r_val["proc_id"] = r_val["interface-id"]
    r_val["@timestamp"] = datetime.fromtimestamp(
        int(r_val["start"])
    )
    return r_val


def lambda_handler(event, context):
    logiq_host = getenv("LOGIQ_HOST")
    SSL = getenv("SSL")
    ingest_token = getenv("INGEST_TOKEN")
    if ingest_token is None:
        raise ValueError("Auth token is missing!")
    payload_data = []
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(ingest_token),
    }
    namespace = (
        getenv("NAMESPACE")
        if getenv("NAMESPACE") is not None
        else "s3-logs"
    )
    cluster_id = (
        getenv("CLUSTER_ID")
        if getenv("CLUSTER_ID") is not None
        else "aws"
    )
    app_name = (
        getenv("APP_NAME")
        if getenv("APP_NAME") is not None
        else "s3-logs"
    )
    logger.info(event)
    event_data = json.loads(event)
    logger.info(event_data)
    payload_data = []
    try:
        if "Records" in event_data:
            records = event_data['Records']
            for record in records:
                if "s3" in record:
                    region = record["awsRegion"]
                    s3 = record["s3"]
                    bucket = ""
                    key = ""
                    if "bucket" in s3:
                        bucket_obj = s3["bucket"]
                        if "name" in bucket_obj:
                            bucket = bucket_obj["name"]

                    if "object" in s3:
                        s3_obj = s3["object"]
                        key = s3_obj["key"]

                    logger.info(bucket + "/" + key)

                    s3_client = boto3.client('s3')

                    local_file_name = "/tmp/" + key
                    file_type = ''
                    if key.endswith(".gz"):
                        file_type = "gz"
                    else:
                        file_type = "txt"

                    logger.info(type)
                    folder_path = local_file_name[:local_file_name.rindex("/")]
                    Path(folder_path).mkdir(parents=True, exist_ok=True)
                    s3_client.download_file(bucket, key, local_file_name)
                    line_num = 0
                    headers = ''
                    log_type = ''

                    file_pointer = None
                    if file_type == "gz":
                        file_pointer = gzip.open(local_file_name, 'rb')
                    else:
                        # assuming file is txt
                        file_pointer = open(local_file_name, "r")

                    for line_bts in file_pointer:
                        if file_type == "gz":
                            line = line_bts.decode("utf-8")
                        else:
                            line = line_bts
                        if line_num == 0:
                            log_type = detect_line(line)

                        if log_type == VPC_FLOW_LOGS:
                            if line_num == 0:
                                headers = line.split()
                                line_num = line_num + 1
                                continue
                            else:
                                processed_flow_logs = process_vpc_flow_logs(line, headers)
                                logger.info(processed_flow_logs)
                                processed_flow_logs["namespace"] = re.sub(pattern, "-", namespace)
                                processed_flow_logs["s3bucket"] = bucket
                                processed_flow_logs["s3key"] = key
                                processed_flow_logs["awsregion"] = region
                                payload_data.append(processed_flow_logs)
                        else:
                            # txt file
                            r_val = dict()
                            r_val["@timestamp"] = datetime.datetime.now(datetime.timezone.utc).isoformat()
                            r_val["message"] = line.replace("\n", "")
                            r_val["s3bucket"] = bucket
                            r_val["namespace"] = re.sub(pattern, "-", bucket)
                            r_val["s3key"] = key
                            r_val["awsregion"] = region
                            r_val["application"] = re.sub(pattern, "-", folder_path)
                            r_val["cluster_id"] = re.sub(pattern, "-", cluster_id),
                            payload_data.append(r_val)
                            logger.info(r_val)

                        if len(payload_data) == 25:
                            http = urllib3.HTTPSConnectionPool(logiq_host, cert_reqs='CERT_NONE', assert_hostname=False)

                            encoded_data = json.dumps(payload_data).encode("utf-8")
                            r = http.request(
                                "POST",
                                "/v1/json_batch".format(logiq_host),
                                body=encoded_data,
                                headers=headers,
                            )
                            logger.info(r.status)
                            payload_data = []

                        line_num = line_num + 1
        return 0

    except Exception as e:
        logger.error(e)
